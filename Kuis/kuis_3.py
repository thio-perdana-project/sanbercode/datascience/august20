import tweepy
import csv
import re
import string
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from datetime import timedelta
import datetime
import sqlite3

class kuis_3:
    def __init__(self, basisdata):
        self.connection = sqlite3.connect(basisdata)

    def insert_clean_tweet(self, tweetid, clean_tweet, sentiment):
        query = '''insert or ignore into clean_tweets (tweetid, clean_tweet, sentiment) values (?,?,?);'''

        cursor = self.connection.cursor()
        cursor.execute(query,(tweetid, clean_tweet, sentiment))
        cursor.close()

    def read_tweet(self):
        query = 'select tweet from tweets;'
        cursor = self.connection.cursor()
        cursor.execute(query)
        hasil = cursor.fetchall()
        cursor.close()
        return hasil
    
    def insert_tweet(self, tweetid, tweet):
        query = '''insert or ignore into tweets (tweetid, tweet) values (?,?);'''

        cursor = self.connection.cursor()
        cursor.execute(query,(tweetid, tweet))
        cursor.close()

    def create_table(self):
        query = 'create table if not exists tweets(tweetid INTEGER PRIMARY KEY, tweet TEXT NOT NULL);'
        cursor = self.connection.cursor()
        cursor.execute(query)
        cursor.close()
    
    def create_clean_table(self):
        query = 'create table if not exists clean_tweets(tweetid INTEGER PRIMARY KEY,clean_tweet TEXT NOT NULL, sentiment INTEGER NOT NULL);'
        cursor = self.connection.cursor()
        cursor.execute(query)
        cursor.close()
    
    def analisis(self,tweet):
        pos_list= open("kata_positif.txt","r")
        pos_kata = pos_list.readlines()
        neg_list= open("kata_negatif.txt","r")
        neg_kata = neg_list.readlines()


        count_p = 0
        count_n = 0
        for kata_pos in pos_kata:
            if kata_pos.strip() in tweet:
                count_p +=1
        for kata_neg in neg_kata:
            if kata_neg.strip() in tweet:
                count_n +=1
        # print ("positif: "+str(count_p))
        # print ("negatif: "+str(count_n))
        hasil = (count_p - count_n)
        # print ("-----------------------------------------------------")
        
        return hasil

    def processing(self):
        data_tweets = self.read_tweet()
        hasil = []
        for data_tweet in data_tweets:
            data_tweet = self.text_processing(data_tweet[0])
            hasil.append(self.analisis(data_tweet))
        
        # hasil = list(filter(lambda num: num != 0, hasil))

        print ("Nilai rata-rata: "+str(np.mean(hasil)))
        print ("Nilai Median: "+str(np.median(hasil)))
        print ("Standar deviasi: "+str(np.std(hasil)))
        print ('\n')


        labels, counts = np.unique(hasil, return_counts=True)
        plt.bar(labels, counts, align='center')
        plt.gca().set_xticks(labels)
        plt.show()

    def text_processing(self, tweet):
        #lowercase
        tweet = tweet.lower()
        #menghapus akun dan link
        tweet = ' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)", " ", tweet).split())
        #menghapus angka
        tweet = re.sub(r"\d+", "", tweet)
        #menghapus tanda baca
        tweet = tweet.translate(str.maketrans("","",string.punctuation))
        #menghapus whitespace
        tweet = tweet.strip()
        return tweet

    def scraping(self, kunci):
        token_data = open("secrettoken.csv")
        tokens = csv.reader(token_data, delimiter=',')

        data_token = []
        for row in tokens:
            data_token.append(row[2])
        
        consumer_key = data_token[0]
        consumer_secret = data_token[1]
        access_token = data_token[2]
        access_token_secret = data_token[3]

        #panggil query autentikasi
        auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
        auth.set_access_token(access_token, access_token_secret)
        api = tweepy.API(auth)

        new_search = kunci + " -filter:retweets"

        tweets = tweepy.Cursor(api.search,
            q=new_search,
            lang="id",
            tweet_mode="extended").items(1000)
        
        for tweet in tweets:
            tweetid = tweet._json['id']
            text = tweet.full_text
            self.insert_tweet(tweetid, text)

    def commit_data(self):
        self.connection.commit()

    def close_connect(self):
        self.connection.close()

    def take_data(self, kata_kunci):
        self.create_table()
        self.create_clean_table()
        self.scraping(kata_kunci)
        self.commit_data()
        self.close_connect()

basisdata = 'tweets.db'
kata_kunci = 'psbb jakarta'

kuis = kuis_3(basisdata)
kuis.take_data(kata_kunci)

# kuis.processing()