import csv
from openpyxl import load_workbook, Workbook
import pandas as pd
from openpyxl.chart import BarChart, Series, Reference
import numpy as np

class kuis_1:
    def __init__(self,file1,file2):
        self.file1 = file1
        self.file2 = file2

    def proses(self):
        raw_data_1 = open(self.file1)
        data_1 = list(csv.reader(raw_data_1, delimiter=','))

        items_1 = []
        items_2 = []

        column_1 = ['Nama Provinsi','GDP']
        column_2 = ['Nama Provinsi','HDI']

        for row_data in data_1:
            if row_data[3] == "NA.GDP.EXC.OG.CR":
                item=[]
                item.append(row_data[0])
                try:
                    item.append(float(row_data[38]))
                except:
                    item.append(0)
                items_1.append(item)
            elif row_data[3] == "IDX.HDI":
                item=[]
                item.append(row_data[0])
                try:
                    item.append(float(row_data[38]))
                except:
                    item.append(0)
                items_2.append(item)

        df_1 = pd.DataFrame(items_1, columns=column_1)
        df_2 = pd.DataFrame(items_2, columns=column_2)

        df = pd.merge(df_1,df_2,on='Nama Provinsi')
        print(df.head())

        return df

        

    def visualisasi(self,df_input):
        df = df_input
        max_value = df['GDP'].max()
        min_value = df['GDP'].min()

        df['GDP'] = df['GDP'].apply(lambda x: (x - min_value) / (max_value-min_value))

        df.rename(columns={'GDP': 'GDP Normalisasi'},inplace=True)

        df.to_excel(self.file2,index=False)
        wb = load_workbook(filename=self.file2)
        ws = wb.active

        ws.column_dimensions['A'].width = 32
        ws.column_dimensions['B'].width = 18

        chart1 = BarChart()
        chart1.type = "col"
        chart1.style = 5
        chart1.title = "GDP Provinsi"
        chart1.y_axis.title = 'Total GDP'
        chart1.x_axis.title = 'Provinsi'
        data_1 = Reference(ws, min_col=2, min_row=1, max_row=len(df.index), max_col=2)
        cats_1 = Reference(ws, min_col=1, min_row=2, max_row=len(df.index))
        chart1.height = 10 # default is 7.5
        chart1.width = 30 # default is 15
        chart1.add_data(data_1, titles_from_data=True)
        chart1.set_categories(cats_1)

        chart2 = BarChart()
        chart2.type = "col"
        chart2.style = 7
        chart2.title = "HDI"
        chart2.y_axis.title = 'HDI'
        chart2.x_axis.title = 'Provinsi'
        chart2.y_axis.scaling.min = 60
        data_2 = Reference(ws, min_col=3, min_row=1, max_row=len(df.index), max_col=3)
        cats_2 = Reference(ws, min_col=1, min_row=2, max_row=len(df.index))
        chart2.height = 10 # default is 7.5
        chart2.width = 30 # default is 15
        chart2.add_data(data_2, titles_from_data=True)
        chart2.set_categories(cats_2)

        ws.add_chart(chart1, "E2")
        ws.add_chart(chart2, "E22")

        wb.save(file2)


file1 = "raw_data.csv"
file2 = "kuis_1.xlsx"

app = kuis_1(file1,file2)
proses = app.proses()
visualisasi = app.visualisasi(proses)







