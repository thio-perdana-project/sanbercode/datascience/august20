from urllib.request import urlopen, Request
from bs4 import BeautifulSoup
import pandas as pd
import matplotlib.pyplot as plt
import sqlite3
import numpy as np

class kuis_2:
    def __init__(self,basisdata):
        self.connection = sqlite3.connect(basisdata)

    def scraping(self,alamat):
        html = urlopen(alamat)
        data = BeautifulSoup(html, 'html.parser')

        tables = data.find('form')
        table = tables.find('table',{'id':'ReportTable'})
        rows = table.findAll('tr')

        table_data = []
        for row in rows:
            # print(row)
            row_data = []
            cell = row.findAll('td')
            if cell[0].get_text().strip() == 'No':
                continue
            else: 
                row_data.append(cell[1].get_text().strip())
                row_data.append(cell[2].get_text().strip().replace(' WIB',''))
                koordinat = cell[3].get_text().strip()
                row_data.append(float(koordinat.split(",")[0]))
                row_data.append(float(koordinat.split(",")[1]))
                row_data.append(float(cell[4].get_text().strip().replace(' SR','')))
                row_data.append(int(cell[5].get_text().strip().lower().replace(' km','')))
                row_data.append(cell[6].get_text().strip())
                table_data.append(row_data)
        return table_data

    def create_table(self):
        query = 'create table if not exists datagempa(id INTEGER PRIMARY KEY AUTOINCREMENT, tanggal TEXT NOT NULL, waktu TEXT NOT NULL, bujur INTEGER, lintang INTEGER, magnitude INTEGER,  kedalaman INTEGER, lokasi TEXT);'
        cursor = self.connection.cursor()
        cursor.execute(query)
        cursor.close()

    def add_data(self,tanggal, waktu, bujur, lintang, magnitude, kedalaman, lokasi):
        query = 'insert or ignore into datagempa(tanggal, waktu, bujur, lintang, magnitude, kedalaman, lokasi) values (?,?,?,?,?,?,?);'
        cursor = self.connection.cursor()
        cursor.execute(query,(tanggal, waktu, bujur, lintang, magnitude, kedalaman, lokasi))
        cursor.close()

    def input_data(self,alamat):
        list_data = self.scraping(alamat)
        self.create_table()

        for data in list_data:
            self.add_data(data[0],data[1],data[2],data[3],data[4],data[5],data[6])

        self.commit_data()
        self.close_connect()

    def lon_lat_data(self):
        query = 'select bujur,lintang from datagempa;'
        cursor = self.connection.cursor()
        cursor.execute(query)
        hasil = cursor.fetchall()
        cursor.close()
        return hasil
    
    def mag_deep_data(self):
        query = 'select magnitude,kedalaman from datagempa;'
        cursor = self.connection.cursor()
        cursor.execute(query)
        hasil = cursor.fetchall()
        cursor.close()
        return hasil

    def visualisasi(self):
        bujur = [data[0] for data in self.lon_lat_data()]
        lintang = [data[1] for data in self.lon_lat_data()]
        magnitude = [data[0] for data in self.mag_deep_data()]
        kedalaman = [data[1] for data in self.mag_deep_data()]

        print('Kedalaman rata-rata gempa adalah',np.mean(kedalaman),'Meter')
        print('Dengan kedalaman maksimum adalah',np.max(kedalaman),'Meter')
        print('Magnitudo rata-rata gempa adalah',np.mean(magnitude),'Magnitudo')
        print('Dengan magnitudo maksimum adalah',np.max(magnitude),'Magnitudo')

        plt.figure(1)
        plt.scatter(bujur,lintang,s=10)
        axes = plt.gca()
        plt.axis('scaled')
        plt.title('Peta Sebaran Gempa di Indonesia')
        axes.set_ylim([-11,6])
        axes.set_xlabel('Garis Bujur')
        axes.set_ylabel('Garis Lintang')
        axes.set_xlim([95,141])

        plt.figure(2)
        axes = plt.gca()
        axes.set_xlabel('Magnitudo')
        axes.set_ylabel('Kedalaman')
        plt.title('Perbandingan Magnitudo terhadap Kedalaman Gempa')
        plt.scatter(magnitude,kedalaman,s=10)

        plt.show()

    def commit_data(self):
        self.connection.commit()

    def close_connect(self):
        self.connection.close()


alamat = 'http://geospasial.bnpb.go.id/pantauanbencana/data/datagempaall.php'
basisdata = 'kuis_2.db'
kuis = kuis_2(basisdata)

# kuis.input_data(alamat)    
kuis.visualisasi()