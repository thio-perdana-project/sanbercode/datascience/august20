import csv
import pandas as pd
import re,string
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer, TfidfTransformer
from sklearn.naive_bayes import GaussianNB
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
import pickle

#membaca file v
#bersihkan filenya v
#analisis datanya (konversi numeric, jalankan machine learning)
#evaluasi

class analisis_sentimen:
    def __init__(self,model_func,idf_func):
        self.model_func = model_func
        self.idf_func = idf_func

    def open_file(self,file):
        token_data = open(file)
        tokens = csv.reader(token_data, delimiter=';')
        tweets = []
        label = []
        sentimen = []
        for row in tokens:
            tweets.append(row[0])
            label.append(int(row[1].replace(',','')))
            if int(row[1].replace(',','')) == 1:
                sentimen.append('Positif')
            else:
                sentimen.append('Negatif')

        df = pd.DataFrame(columns=['tweets','label'])
        df['tweets'] = tweets
        df['label'] = label
        df['sentimen'] = sentimen
        return df

    def proses(self, tweets):
        clean_tweets = []

        count = 0

        for tweet in tweets:
            def hapus_tanda(tweet): 
                tanda_baca = set(string.punctuation)
                tweet = ''.join(ch for ch in tweet if ch not in tanda_baca)
                return tweet
            
            def hapus_katadouble(s): 
                pattern = re.compile(r"(.)\1{1,}", re.DOTALL)
                return pattern.sub(r"\1\1", s)

            tweet=tweet.lower()
            tweet=tweet.replace('\n',' ')
            tweet=tweet.strip()
            tweet = re.sub(r'\\u\w\w\w\w', '', tweet)
            tweet=re.sub(r'http\S+','',tweet)
            #hapus @username
            tweet=re.sub('@[^\s]+','',tweet)
            #hapus #tagger 
            tweet = re.sub(r'#([^\s]+)', r'\1', tweet)
            #hapus tanda baca
            tweet=hapus_tanda(tweet)
            #hapus angka dan angka yang berada dalam string 
            tweet=re.sub(r'\w*\d\w*', '',tweet).strip()
            #hapus repetisi karakter 
            tweet=hapus_katadouble(tweet)
            #stemming
            factory = StemmerFactory()
            stemmer = factory.create_stemmer()
            tweet = stemmer.stem(tweet)
            clean_tweets.append(tweet)
            count += 1
            print ('proses data ke',count)

        return clean_tweets

    def analisis(self,data):
        df = data
        vectorizer = TfidfVectorizer (max_features=2500)
        model_g = GaussianNB()

        v_data = vectorizer.fit_transform(df['clean_tweets']).toarray()

        X_train, X_test, y_train, y_test = train_test_split(v_data, df['label'], test_size=0.2, random_state=0)
        model_g.fit(X_train,y_train)

        pickle.dump(vectorizer, open(self.idf_func, 'wb'))
        pickle.dump(model_g, open(self.model_func, 'wb'))

        y_preds = model_g.predict(X_test)

        print(confusion_matrix(y_test,y_preds))
        print(classification_report(y_test,y_preds))
        print('nilai akurasinya adalah ',accuracy_score(y_test, y_preds))

    def prediksi(self, kalimat):
        vector = pickle.load(open(self.idf_func, 'rb'))
        model = pickle.load(open(self.model_func, 'rb'))

        v_data_2 = vector.transform(kalimat).toarray()
        hasil = model.predict(v_data_2)

        return hasil


file = 'dataset_tweet_2.csv'
model = 'm_func.sav'
vector = 'v_func.sav'

#inisiasi class
kerja = analisis_sentimen(model,vector)
#buka file
data = kerja.open_file(file)
data_tweets = data['tweets'].values
#pembersihan data
data['clean_tweets'] = kerja.proses(data_tweets)
#analisis data
kerja.analisis(data)

#prediksi
# kalimat ='presiden jokowi memiliki kinerja yang baik'
# kalimat = kerja.proses([kalimat])
# hasil = kerja.prediksi(kalimat)

# if hasil[0] == 1:
#     print('Positif')
# else:
#     print('Negatif')
